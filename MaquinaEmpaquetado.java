package ito.poo.app;
import java.time.LocalDate;
public class MaquinaEmpaquetado extends Maquinas {
	private int Tipo;
	private int CantxMin;
	
	public MaquinaEmpaquetado(LocalDate FechaAd, float Costo, int Tipo, int CantxMin) {
		super(FechaAd,Costo);
		this.Tipo=Tipo;
		this.CantxMin=CantxMin;
	}
	
	public float CostoEmpaquetado(float costob) {
		return (costob*0.06f)/27;
	}

	public int getTipo() {
		return Tipo;
	}

	public void setTipo(int tipo) {
		Tipo = tipo;
	}

	public int getCantxMin() {
		return CantxMin;
	}

	public void setCantxMin(int cantxMin) {
		CantxMin = cantxMin;
	}

	@Override
	public String toString() {
		return "MEmpaquetado [Tipo=" + Tipo + ", CantxMin=" + CantxMin + "]";
	}

}
