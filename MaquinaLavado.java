package ito.poo.app;
import java.time.LocalDate;
public class MaquinaLavado extends Maquinas{
	private float CapacidadL;
	private float TiempoxBotella;
	
	public MaquinaLavado(LocalDate FechaAd, float Costo, float CapacidadL, float TiempoxBotella) {
		super(FechaAd,Costo);
		this.CapacidadL=CapacidadL;
		this.TiempoxBotella=TiempoxBotella;
	}
	
	public float CostoLavado(float costob) {
		return (costob*0.5f)/3;
	}

	public float getCapacidadL() {
		return CapacidadL;
	}

	public void setCapacidadL(float capacidadL) {
		CapacidadL = capacidadL;
	}

	public float getTiempoxBotella() {
		return TiempoxBotella;
	}

	public void setTiempoxBotella(float tiempoxBotella) {
		TiempoxBotella = tiempoxBotella;
	}

	@Override
	public String toString() {
		return "MLavado [CapacidadL=" + CapacidadL + ", TiempoxBotella=" + TiempoxBotella + "]";
	}
	
}